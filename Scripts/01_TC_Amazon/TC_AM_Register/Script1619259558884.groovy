import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject

import java.util.regex.Pattern

import org.openqa.selenium.By as By
import org.openqa.selenium.By.ByClassName as ByClassName
import org.openqa.selenium.By.ById as ById
import org.openqa.selenium.By.ByXPath as ByXPath
import org.openqa.selenium.WebDriver as WebDriver
import org.openqa.selenium.WebElement as WebElement
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.common.WebUiCommonHelper as WebUiCommonHelper
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

WebUI.openBrowser('https://www.amazon.com')

WebUI.maximizeWindow()

'------------------------------------------- Homepage --------------------------------------------'
WebUI.click(findTestObject('Object Repository/HomePage/button_close_pop_up'))

WebUI.click(findTestObject('Object Repository/HomePage/tab_registry'))

'------------------------------------------- Registry and Gifting Page --------------------------------------------'
WebUI.verifyElementPresent(findTestObject('AmazonRegistryGifting/page_header'), 3)

WebUI.setText(findTestObject('Object Repository/AmazonRegistryGifting/input_registrant_name'), registrant_name)

WebUI.click(findTestObject('Object Repository/AmazonRegistryGifting/select_registry_gift_type'))

WebUI.click(findTestObject('Object Repository/AmazonRegistryGifting/registry_gift_type'))

WebUI.click(findTestObject('Object Repository/AmazonRegistryGifting/button_search_registry'))

'------------------------------------------- Search Result Page --------------------------------------------'
WebUI.click(findTestObject('Object Repository/SearchResultPage/select_month_from'))

WebUI.click(findTestObject('SearchResultPage/month_from'))

WebUI.click(findTestObject('Object Repository/SearchResultPage/select_year_from'))

WebUI.click(findTestObject('SearchResultPage/year_from'))

WebUI.click(findTestObject('Object Repository/SearchResultPage/select_month_to'))

WebUI.click(findTestObject('SearchResultPage/month_to'))

WebUI.click(findTestObject('Object Repository/SearchResultPage/select_year_to'))

WebUI.click(findTestObject('SearchResultPage/year_to'))

WebUI.click(findTestObject('Object Repository/SearchResultPage/button_search_for_birthday_gift_list'))

'------------------------------------------- Verify Search Result --------------------------------------------'
WebDriver driver = DriverFactory.getWebDriver()
table = driver.findElements(By.xpath('//*[@id="search-result-container"]/li/div[@class="gr-search-registry-date"]')).size()
println(table)

String[] expected = ['Jan', 'Feb', 'Mar', 'Apr']

for (int i = 2; i <= table; i++) {
    table_value = WebUI.getText(findTestObject('Object Repository/SearchResultPage/table_row', [('id') : i]))
	
	for (int j = expected.length - 1; j >= 0; --j) {
		if (table_value.indexOf(expected[j]) != -1) {
		WebUI.verifyElementPresent(findTestObject('Object Repository/SearchResultPage/verify_search_result', [('id') : i, ('verify_search_result') : expected[j]]),5)
		WS.comment("your table with registry date : "+table_value+" contains expected month : "+expected[j])
		WS.comment(expected[j])
		}
	}
}
